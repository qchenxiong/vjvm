package vjvm;

import java.io.IOException;
import vjvm.runtime.VClassLoader;
import vjvm.runtime.VExecutor;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class VJVM {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        VClassLoader.LoadClass("/home/vin/temp/JPF/ClassFileFormat.class");
        VClassLoader.LoadClass("/home/vin/temp/JPF/ClassA.class");
        
        VExecutor.getInstance().start("ClassFileFormat", "main");
    }
}
