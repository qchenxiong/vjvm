/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.runtime;

import java.util.ArrayList;
import vjvm.instructions.VInstruction;

/**
 *
 * @author vin
 */
public class VStackFrame {
    private int frameStack[];
    private int max_stack;
    private int max_locals;
    private int operandPointer;
    private int pc;
    private ArrayList<VInstruction> instructions;
    private boolean ret = false;
    
    public VStackFrame(short max_stack, short max_locals, ArrayList<VInstruction> instructions){
        this.max_stack = max_stack;
        this.max_locals = max_locals;
        frameStack = new int[max_stack+max_locals];
        operandPointer = this.max_locals-1;
        this.instructions = instructions;
        pc = 0;
    }
    
    public void start(){
        while(!ret){
            instructions.get(pc).execute(this);
        }
        printRestStacks();
    }
    
    private void printRestStacks(){
        for(int i = max_locals; i < operandPointer; i++){
            System.out.println(frameStack[i]);
        }
    }
    
    public void stop(){
        ret = true;
    }
    
    public void forward(){
        pc++;
    }
    
    public void jump(int offset){
        while(offset < 0){
            offset = offset + instructions.get(--pc).getByteSize(); 
        }
        while(offset > 0){
            offset = offset - instructions.get(pc++).getByteSize();
        }
    }
    
    
    
    public void pushOperandChar(char c){
        frameStack[++operandPointer] = c;
    }
    
    public char popOperandChar(){
        return (char) frameStack[operandPointer--];
    }
    
    public void pushOperandByte(byte b){
        frameStack[++operandPointer] = b;
    }
    
    public byte popOperandByte(){
        return (byte) frameStack[operandPointer--];
    }
    
    public void pushOperandBoolean(boolean b){
        frameStack[++operandPointer] = b ? 1 : 0;
    }
    
    public boolean popOperandBoolean(){
        return frameStack[operandPointer--]==1 ? true : false;
    }
    
    public void pushOperandShort(short s){
        frameStack[++operandPointer] = s;
    }
    
    public short popOperandShort(){
        return (short) frameStack[operandPointer--];
    }
    
    public void pushOperandReference(int ref){
        frameStack[++operandPointer] = ref;
    }
    
    public int popOperandReference(){
        return frameStack[operandPointer--];
    }
    
    public void pushOperandInt(int i){
        frameStack[++operandPointer] = i;
    }
    
    public int popOperandInt(){
        return frameStack[operandPointer--];
    }
    
    public void pushOperandFloat(float f){
        frameStack[++operandPointer] = Float.floatToIntBits(f);
    }
    
    public float popOperandFloat(){
        return Float.intBitsToFloat(frameStack[operandPointer--]);
    }
    
    public void pushOperandLong(long l){
        frameStack[++operandPointer] = (int) (l >> 32);
        frameStack[++operandPointer] = (int) l;
    }
    
    public long popOperandLong(){
        return ((long)frameStack[operandPointer--] & 0xffffffff) + ((long)frameStack[operandPointer--] << 32);
    }
    
    public void pushOperandDouble(double d){
        pushOperandLong(Double.doubleToLongBits(d));
    }
    
    public double popOperandDouble(){
        return Double.longBitsToDouble(popOperandLong());
    }
    
    public void storeChar(int index, char c){
        frameStack[index] = c;
    }
    
    public char loadChar(int index){
        return (char) frameStack[index];
    }
    
    public void storeByte(int index, byte b){
        frameStack[index] = b;
    }
    
    public byte loadByte(int index){
        return (byte) frameStack[index];
    }
    
    public void storeBoolean(int index, boolean b){
        frameStack[index] = b ? 1 : 0;
    }
    
    public boolean loadBoolean(int index){
        return frameStack[index] == 1 ? true : false;
    }
    
    public void storeShort(int index, short s){
        frameStack[index] = s;
    }
    
    public short loadShort(int index){
        return (short) frameStack[index];
    }
    
    public void storeInt(int index, int i){
        frameStack[index] = i;
    }
    
    public int loadInt(int index){
        return frameStack[index];
    }
    
    public void storeReference(int index, int ref){
        frameStack[index] = ref;
    }
    
    public int loadReference(int index){
        return frameStack[index];
    }
    
    public void storeFloat(int index, float f){
        frameStack[index] = Float.floatToIntBits(f);
    }
    
    public float loadFloat(int index){
        return Float.intBitsToFloat(frameStack[index]);
    }
    
    public void storeLong(int index, long l){
        frameStack[index] = (int) (l >> 32);
        frameStack[index+1] = (int) l;
    }
    
    public long loadLong(int index){
        return ((long)frameStack[index] << 32) + ((long)frameStack[index+1]) & 0xffffffff;
    }
    
    public void storeDouble(int index, double d){
        storeLong(index, Double.doubleToLongBits(d));
    }
    
    public double loadDouble(int index){
        return Double.longBitsToDouble(loadLong(index));
    }
}
