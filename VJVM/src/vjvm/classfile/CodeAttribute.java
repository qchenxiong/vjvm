package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-19
 * @author vincent
 */
public class CodeAttribute extends AttributeInfo{
    //appear int non-native and non-abstract MethodInfo
    
    short max_stack;
    short max_locals;
    int code_length;
    byte code[];
    short exception_table_length;
    ExceptionTable exception_table[];
    short attributes_count;
    AttributeInfo attributes[];
    
    public CodeAttribute(DataInputStream stream, short name_index) throws IOException{
        this.attribute_name_index = name_index;
        attribute_length = stream.readInt();
        max_stack = stream.readShort();
        max_locals = stream.readShort();
        code_length = stream.readInt(); 
        if(code_length > 0){
            code = new byte[code_length]; 
            stream.read(code);
        }else{
            code = null;
        }
        
        exception_table_length = stream.readShort(); 
        if(exception_table_length > 0){
            exception_table = new ExceptionTable[exception_table_length];
            for(int i = 0; i < exception_table_length; i++){
                exception_table[i] = new ExceptionTable(stream);
            }
        }
                    
        //ignore the code attributes
        attributes_count = stream.readShort();
        if(attributes_count > 0){
            for(int j = 0; j < attributes_count; j++){
                stream.skipBytes(2);
                int attribute_length = stream.readInt();
                stream.skipBytes(attribute_length);
            }
        }
     }
}
