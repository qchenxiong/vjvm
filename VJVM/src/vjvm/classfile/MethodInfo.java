package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class MethodInfo {

    short access_flags;
    short name_index;
    short descriptor_index;
    short attributes_count;
    AttributeInfo attributes[];
    
    private int size;
    private CodeAttribute codeAttribute;
    private JavaClass JC;
    
    public MethodInfo(DataInputStream stream, JavaClass JC) throws IOException{
        this.JC = JC;
        size = 0;
        access_flags = stream.readShort();
        name_index = stream.readShort();
        descriptor_index = stream.readShort();
        attributes_count = stream.readShort(); size += 8;
        
        if(attributes_count > 0){
            attributes = new AttributeInfo[attributes_count];
            for(int i = 0; i < attributes_count; i++){
                short attribute_name_index = stream.readShort(); size += 2;
                String attribute_name = JC.getStringFromCP(attribute_name_index);
                if(attribute_name.equals("Code")){
                    attributes[i] = new CodeAttribute(stream, attribute_name_index);
                    codeAttribute = (CodeAttribute) attributes[i];
                    size += 4 + codeAttribute.attribute_length;
                }else{
                    //ignore the non-code attributes
                    int attribute_length = stream.readInt();
                    stream.skipBytes(attribute_length); size += 4 + attribute_length;
                }
            }
        }
    }
    
    public CodeAttribute getCodeAttribute(){
        return codeAttribute;
    }
    
    public int getSize(){
        return size;
    }
}
