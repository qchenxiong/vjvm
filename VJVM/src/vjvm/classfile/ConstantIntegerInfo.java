package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class ConstantIntegerInfo extends CPInfo{

    int value;
    
    public ConstantIntegerInfo(byte tag, DataInputStream stream) throws IOException{
        this.tag = tag;
        value = stream.readInt();
    }

    @Override
    public int getSize() {
        return 5;
    }
}
