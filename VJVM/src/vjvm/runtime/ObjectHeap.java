/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.runtime;

import java.util.ArrayList;
import vjvm.classfile.JavaClass;
import vjvm.type.*;

/**
 *
 * @author vin
 */
public class ObjectHeap {
    ArrayList<VObject> objectList;
    private static ObjectHeap instance;
    
    private ObjectHeap(){
        objectList = new ArrayList<VObject>();
    }
    
    public static ObjectHeap getInstance(){
        if(instance == null){
            instance = new ObjectHeap();
        }
        return instance;
    }
    
    public int addObject(VObject vobj){
        objectList.add(vobj);
        return (objectList.size()-1);
    }
    
    public JavaClass getClassByObjectRef(int objRef){
        return objectList.get(objRef).getJavaClass();
    }
    
    public void putBField(int objRef, int fieldIndex, byte b){
        ((ByteType)objectList.get(objRef).getField(fieldIndex)).setValue(b);
    }
    
    public void putCField(int objRef, int fieldIndex, char c){
        ((CharType)objectList.get(objRef).getField(fieldIndex)).setValue(c);
    }
    
    public void putDField(int objRef, int fieldIndex, double d){
        ((DoubleType)objectList.get(objRef).getField(fieldIndex)).setValue(d);
    }
    
    public void putFField(int objRef, int fieldIndex, float f){
        ((FloatType)objectList.get(objRef).getField(fieldIndex)).setValue(f);
    }
    
    public void putIField(int objRef, int fieldIndex, int i){
        ((IntType)objectList.get(objRef).getField(fieldIndex)).setValue(i);
    }
    
    public void putJField(int objRef, int fieldIndex, long j){
        ((LongType)objectList.get(objRef).getField(fieldIndex)).setValue(j);
    }
    
    public void putLField(int objRef, int fieldIndex, int l){
        ((ReferenceType)objectList.get(objRef).getField(fieldIndex)).setValue(l);
    }
    
    public void putSField(int objRef, int fieldIndex, short s){
        ((ShortType)objectList.get(objRef).getField(fieldIndex)).setValue(s);
    }
    
    public void putZField(int objRef, int fieldIndex, boolean z){
        ((BooleanType)objectList.get(objRef).getField(fieldIndex)).setValue(z);
    }
}
