/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vastore extends VInstruction{
    byte vindex;
    
    public Vastore(byte byte1){
        this.vindex = byte1;
    }

    @Override
    public void execute(VStackFrame frame) {
        int objRef = frame.popOperandReference();
        frame.storeReference(vindex, objRef);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 2;
    }

}
