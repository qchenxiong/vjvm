/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.controlflow;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vgoto extends VInstruction{
    byte branchbyte1, branchbyte2;
    int offset;
    
    public Vgoto(byte byte1, byte byte2){
        this.branchbyte1 = byte1;
        this.branchbyte2 = byte2;
        offset = (byte1 << 8) + (((int)byte2) & 0xff);
    }

    @Override
    public void execute(VStackFrame frame) {
        frame.jump(offset);
    }

    @Override
    public int getByteSize() {
        return 3;
    }
    
}
