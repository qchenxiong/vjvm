/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.objectsandarrays;

import vjvm.classfile.JavaClass;
import vjvm.instructions.VInstruction;
import vjvm.runtime.ObjectHeap;
import vjvm.runtime.VExecutor;
import vjvm.runtime.VStackFrame;
import vjvm.type.*;

/**
 *
 * @author vin
 */
public class Vputfield extends VInstruction{
    byte indexbyte1, indexbyte2;
    
    public Vputfield(byte byte1, byte byte2){
        this.indexbyte1 = byte1;
        this.indexbyte2 = byte2;
    }

    @Override
    public void execute(VStackFrame frame) {
        int fieldRefIndex = ((int)indexbyte1 << 8) + ((int)indexbyte2 & 0xff);
        JavaClass jc = VExecutor.currentJC;
        int fieldIndex = jc.getFieldIndexByFieldrefIndex(fieldRefIndex);
        String typeDesc = jc.getTypeDescriptorByFieldrefIndex(fieldRefIndex);
        int objRef;
        switch(typeDesc.charAt(0)){
            case 'B':
                byte b = frame.popOperandByte();
                objRef = frame.popOperandReference();
                ObjectHeap.getInstance().putBField(objRef, fieldIndex, b);
                break;
            case 'C':
                char c = frame.popOperandChar();
                objRef = frame.popOperandReference();
                ObjectHeap.getInstance().putCField(objRef, fieldIndex, c);
                break;
            case 'D':
                double d = frame.popOperandDouble();
                objRef = frame.popOperandReference();
                ObjectHeap.getInstance().putDField(objRef, fieldIndex, d);
                break;
            case 'F':
                float f = frame.popOperandFloat();
                objRef = frame.popOperandReference();
                ObjectHeap.getInstance().putFField(objRef, fieldIndex, f);
                break;
            case 'I':
                int i = frame.popOperandInt();
                objRef = frame.popOperandInt();
                ObjectHeap.getInstance().putIField(objRef, fieldIndex, i);
                break;
            case 'J':
                long j = frame.popOperandLong();
                objRef = frame.popOperandReference();
                ObjectHeap.getInstance().putJField(objRef, fieldIndex, j);
                break;
            case 'L':
                int l = frame.popOperandReference();
                objRef = frame.popOperandReference();
                ObjectHeap.getInstance().putLField(objRef, fieldIndex, l);
                break;
            case 'S':
                short s = frame.popOperandShort();
                objRef = frame.popOperandReference();
                ObjectHeap.getInstance().putSField(objRef, fieldIndex, s);
                break;
            case 'Z':
                boolean z = frame.popOperandBoolean();
                objRef = frame.popOperandReference();
                ObjectHeap.getInstance().putZField(objRef, fieldIndex, z);
                break;
            default:
                System.err.println("not support array yet");
                System.exit(0);
                break;
        }
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 3;
    }
    
}
