/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vdup extends VInstruction{

    @Override
    public void execute(VStackFrame frame) {
        int i = frame.popOperandInt();
        frame.pushOperandInt(i);
        frame.pushOperandInt(i);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 1;
    }
    
}
