/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.integerarithmetic;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vladd extends VInstruction{

    @Override
    public void execute(VStackFrame frame) {
        long l1 = frame.popOperandLong();
        long l2 = frame.popOperandLong();
        long result = l1 + l2;
        frame.pushOperandLong(result);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 1;
    }
}
