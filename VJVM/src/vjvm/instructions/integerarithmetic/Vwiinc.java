/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.integerarithmetic;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vwiinc extends VInstruction{
    byte indexbyte1, indexbyte2, constbyte1, constbyte2;
    
    public Vwiinc(byte byte1, byte byte2, byte byte3, byte byte4){
        this.indexbyte1 = byte1;
        this.indexbyte2 = byte2;
        this.constbyte1 = byte3;
        this.constbyte2 = byte4;
    }

    @Override
    public void execute(VStackFrame frame) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getByteSize() {
        return 5;
    }
    
}
