package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class ConstantDoubleInfo extends CPInfo{

    double value;
    
    public ConstantDoubleInfo(byte tag, DataInputStream stream) throws IOException{
        this.tag = tag;
        value = stream.readDouble();
    }

    @Override
    public int getSize() {
        return 9;
    }
}
