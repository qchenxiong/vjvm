/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.type;

/**
 *
 * @author vin
 */
public class ShortType extends PrimitiveType{
    short value;
    
    public ShortType(){
        this.descriptor = "S";
    }
    
    public void setValue(short s){
        this.value = s;
    }
    
    public short getValue(){
        return value;
    }
}
