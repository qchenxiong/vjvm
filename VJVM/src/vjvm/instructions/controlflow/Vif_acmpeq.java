/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.controlflow;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vif_acmpeq extends VInstruction{
    byte branchbyte1, branchbyte2;

    public Vif_acmpeq(byte byte1, byte byte2){
        this.branchbyte1 = byte1;
        this.branchbyte2 = byte2;
    }

    @Override
    public void execute(VStackFrame frame) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getByteSize() {
        return 3;
    }
    
}
