/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.type;

/**
 *
 * @author vin
 */
public class BooleanType extends PrimitiveType{
    private boolean value;
    
    public BooleanType(){
        this.descriptor = "Z";
    }
    
    public void setValue(boolean b){
        this.value = b;
    }
    
    public boolean getValue(){
        return value;
    }
}
