/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.type;

/**
 *
 * @author vin
 */
public class FloatType extends PrimitiveType{
    private float value;
    
    public FloatType(){
        this.descriptor = "F";
    }
    
    public void setValue(float f){
        this.value = f;
    }
    
    public float getValue(){
        return value;
    }
}
