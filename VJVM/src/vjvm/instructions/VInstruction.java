/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions;

import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public abstract class VInstruction {
    public abstract void execute(VStackFrame frame);
    public abstract int getByteSize();
}
