/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vlconst extends VInstruction{
    long n;
    
    public Vlconst(long n){
        this.n = n;
    }

    @Override
    public void execute(VStackFrame frame) {
        frame.pushOperandLong(n);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 1;
    }
    
}
