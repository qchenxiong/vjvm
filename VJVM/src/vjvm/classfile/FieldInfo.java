package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class FieldInfo {

    short access_flags;
    short name_index;
    short descriptor_index;
    short attributes_count;
    AttributeInfo attributes[];
    
    private int size;
    private boolean isConstant;
    private ConstantValueAttribute constantValueAttribute;
    private JavaClass JC;
    
    public FieldInfo(DataInputStream stream, JavaClass JC) throws IOException{
        this.JC = JC;
        size = 0;
        isConstant = false;
        access_flags = stream.readShort();
        name_index = stream.readShort();
        descriptor_index = stream.readShort();
        attributes_count = stream.readShort(); size += 8;
        
        if(attributes_count > 0){
            attributes = new AttributeInfo[attributes_count];
            for(int i = 0; i < attributes_count; i++){
                short attribute_name_index = stream.readShort();
                int attribute_length = stream.readInt(); size += 6;
                if(attribute_length == 2){
                    attributes[i] = new ConstantValueAttribute();
                    attributes[i].attribute_name_index = attribute_name_index;
                    attributes[i].attribute_length = attribute_length;
                    ((ConstantValueAttribute) attributes[i]).constantvalue_index = stream.readShort();
                    isConstant = true;
                    constantValueAttribute = (ConstantValueAttribute)attributes[i];
                }else{
                    stream.skipBytes(attribute_length);
                }
                size += attribute_length;
            }
        }
    }
    
    public int getSize(){
        return size;
    }
}
