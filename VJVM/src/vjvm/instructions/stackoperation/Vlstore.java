/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vlstore extends VInstruction{
    byte vindex;
    
    public Vlstore(byte byte1){
        this.vindex = byte1;
    }

    @Override
    public void execute(VStackFrame frame) {
        long l = frame.popOperandLong();
        frame.storeLong(vindex, l);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 2;
    }

}
