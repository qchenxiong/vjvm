/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.runtime;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vin
 */
public class VStack {
    private List<VStackFrame> stackFrames;
    
    //singleton pattern, only support single thread
    private static VStack instance;
    
    private VStack(){
        stackFrames = new ArrayList<VStackFrame>();
    }
    
    public static VStack getInstance(){
        if(instance == null){
            instance = new VStack();
        }
        return instance;
    }
    
    public void addFrame(VStackFrame frame){
        stackFrames.add(frame);
    }
    
    public int getFrameIndex(VStackFrame frame){
        return stackFrames.indexOf(frame);
    }
    
    public VStackFrame getFrame(int index){
        return stackFrames.get(index);
    }
    
    public void popFrame(VStackFrame frame){
        stackFrames.remove(frame);
    }
}
