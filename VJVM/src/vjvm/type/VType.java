/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.type;

import java.util.ArrayList;

/**
 *
 * @author vin
 */
public class VType {
    public String descriptor;
    
    public static ArrayList<VType> getTypeList(String parametersString){
        ArrayList<VType> parameters = new ArrayList<VType>();
        int lb = parametersString.indexOf('(');
        int rb = parametersString.indexOf(')');
        String subString = parametersString.substring(lb+1, rb);
        if(subString != null){
            for(int i = 0; i < subString.length(); i++){
                VType type = null;
                switch(subString.charAt(i)){
                    case 'B':
                        type = new ByteType();
                        break;
                    case 'C':
                        type = new CharType();
                        break;
                    case 'D':
                        type = new DoubleType();
                        break;
                    case 'F':
                        type = new FloatType();
                        break;
                    case 'I':
                        type = new IntType();
                        break;
                    case 'J':
                        type = new LongType();
                        break;
                    case 'S':
                        type = new ShortType();
                        break;
                    case 'Z':
                        type = new BooleanType();
                        break;
                    case 'L':
                        type = new ReferenceType();
                        int j = subString.indexOf(';', i);
                        type.descriptor = subString.substring(i, j+1);
                        i = j;
                        break;
                    default:
                        System.err.println("not support array yet");
                        System.exit(0);
                        break;
                }
                parameters.add(type);
            }
        }
        return parameters;
    }
    
}
