/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.mthods;

import java.util.ArrayList;
import vjvm.classfile.JavaClass;
import vjvm.instructions.VInstruction;
import vjvm.runtime.ClassHeap;
import vjvm.runtime.ObjectHeap;
import vjvm.runtime.VExecutor;
import vjvm.runtime.VStackFrame;
import vjvm.type.BooleanType;
import vjvm.type.ByteType;
import vjvm.type.CharType;
import vjvm.type.DoubleType;
import vjvm.type.FloatType;
import vjvm.type.IntType;
import vjvm.type.LongType;
import vjvm.type.ReferenceType;
import vjvm.type.ShortType;
import vjvm.type.VType;

/**
 *
 * @author vin
 */
public class Vinvokespecial extends VInstruction{
    byte indexbyte1, indexbyte2;

    public Vinvokespecial(byte byte1, byte byte2){
        this.indexbyte1 = byte1;
        this.indexbyte2 = byte2;
    }

    @Override
    public void execute(VStackFrame frame) {
        int methodrefIndex = ((int)indexbyte1 << 8) + ((int)indexbyte2 & 0xff);
        //prepare the parameters
        String parametersString = VExecutor.currentJC.getMethodParametersByMethodRef(methodrefIndex);
        ArrayList<VType> parameters = VType.getTypeList(parametersString);
        
        for(int i = parameters.size() - 1; i >= 0; i--){
            switch(parameters.get(i).descriptor.charAt(0)){
                case 'B':
                    ByteType bt = (ByteType) parameters.get(i);
                    bt.setValue(frame.popOperandByte());
                    break;
                case 'C':
                    CharType ct = (CharType) parameters.get(i);
                    ct.setValue(frame.popOperandChar());
                    break;
                case 'D':
                    DoubleType dt = (DoubleType) parameters.get(i);
                    dt.setValue(frame.popOperandDouble());
                    break;
                case 'F':
                    FloatType ft = (FloatType) parameters.get(i);
                    ft.setValue(frame.popOperandFloat());
                    break;
                case 'I':
                    IntType it = (IntType) parameters.get(i);
                    it.setValue(frame.popOperandInt());
                    break;
                case 'J':
                    LongType lt = (LongType) parameters.get(i);
                    lt.setValue(frame.popOperandLong());
                    break;
                case 'L':
                    ReferenceType rt = (ReferenceType) parameters.get(i);
                    rt.setValue(frame.popOperandReference());
                    break;
                case 'S':
                    ShortType st = (ShortType) parameters.get(i);
                    st.setValue(frame.popOperandShort());
                    break;
                case 'Z':
                    BooleanType booleanType= (BooleanType) parameters.get(i);
                    booleanType.setValue(frame.popOperandBoolean());
                    break;
            }
        }
        
        //get the objref and add to the 0 index
        int objRef = frame.popOperandReference();
        ReferenceType thisObject = new ReferenceType();
        String className = VExecutor.currentJC.getClassNameByMethodRef(methodrefIndex);
        thisObject.descriptor = "L"+className+";";
        thisObject.setValue(objRef);
        parameters.add(0, thisObject);
        
        //get the method index
        String methodName = VExecutor.currentJC.getMethodNameByMethodRef(methodrefIndex);
        int methodIndex = VExecutor.currentJC.getMethodIndex(methodName);
        
        //not support the inheritance
        String superClass = VExecutor.currentJC.getSuperClassName();
        if(!className.equals(superClass)){
            VExecutor.getInstance().invokeSpecial(methodIndex, parameters);
        }
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 3;
    }
    
}
