/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.integerarithmetic;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Viinc extends VInstruction{
    byte vindex, constbyte;
    
    public Viinc(byte byte1, byte byte2){
        this.vindex = byte1;
        this.constbyte = byte2;
    }

    @Override
    public void execute(VStackFrame frame) {
        int i = frame.loadInt(vindex);
        frame.storeInt(vindex, i+constbyte);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 3;
    }
}
