package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class ConstantFloatInfo extends CPInfo{

    float value;
    
    public ConstantFloatInfo(byte tag, DataInputStream stream) throws IOException{
        this.tag = tag;
        value = stream.readFloat();
    }

    @Override
    public int getSize() {
        return 5;
    }
}
