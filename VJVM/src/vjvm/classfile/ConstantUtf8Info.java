package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class ConstantUtf8Info extends CPInfo{

    short length;
    byte bytes[];
    String s;
    
    public ConstantUtf8Info(byte tag, DataInputStream stream) throws IOException{
        this.tag = tag;
        length = stream.readShort();
        bytes = new byte[length];
        stream.read(bytes);
        s = new String(bytes);
    }

    @Override
    public int getSize() {
        return length + 3;
    }
    
    public String toString(){
        return new String(bytes);
    }
}
