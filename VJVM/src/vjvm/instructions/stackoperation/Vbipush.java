/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vbipush extends VInstruction{
    byte byte1;

    public Vbipush(byte byte1){
        this.byte1 = byte1;
    }

    @Override
    public void execute(VStackFrame frame) {
        frame.pushOperandInt(byte1);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 2;
    }

}
