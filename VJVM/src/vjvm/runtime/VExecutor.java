/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.runtime;

import java.util.ArrayList;
import vjvm.classfile.JavaClass;
import vjvm.instructions.VInstruction;
import vjvm.instructions.VInstructionFactory;
import vjvm.type.*;

/**
 *
 * @author vin
 */
public class VExecutor {
    public final String CONSTRUCTOR_METHOD = "<init>";
    private static VExecutor instance;
    public static JavaClass currentJC;
    
    private VExecutor(){
        
    }
    
    public static VExecutor getInstance(){
        if(instance == null){
            instance = new VExecutor();
        }
        return instance;
    }
    
    //invoke the static main method
    public void start(String className, String methodName){
        JavaClass jc = ClassHeap.getInstance().getClass(className);
        int mainIndex = jc.getMethodIndex("main");
        ArrayList<VType> parameters = new ArrayList<VType>();
        invokeStatic(jc, mainIndex, parameters);
    }
    
    public void invokeStatic(JavaClass jc, int methodIndex, ArrayList<VType> parameters){
        currentJC = jc;
        byte[] codes = jc.getMethodCodes(methodIndex);
        short max_stack = jc.getMethodStackLength(methodIndex);
        short max_locals = jc.getMethodMaxLocals(methodIndex);
        ArrayList<VInstruction> instructions = VInstructionFactory.getInstructions(codes);
        VStackFrame frame = new VStackFrame(max_stack, max_locals, instructions);
        
        //set the parameters
        setParameters(frame, parameters);
        
        //add the frame to the stack
        VStack.getInstance().addFrame(frame);
        
        //start the method
        frame.start();
    }
    
    public void invokeSpecial(int methodIndex, ArrayList<VType> parameters){
        //this
        int objRef = ((ReferenceType)parameters.get(0)).getValue();
        
        //the class
        JavaClass jc = ObjectHeap.getInstance().getClassByObjectRef(objRef);
        currentJC = jc;
        
        //get the instructions
        byte[] codes = jc.getMethodCodes(methodIndex);
        short max_stack = jc.getMethodStackLength(methodIndex);
        short max_locals = jc.getMethodMaxLocals(methodIndex);
        ArrayList<VInstruction> instructions = VInstructionFactory.getInstructions(codes);
        VStackFrame frame = new VStackFrame(max_stack, max_locals, instructions);
        
        //set the parameters
        setParameters(frame, parameters);
        
        //add the frame to the stack
        VStack.getInstance().addFrame(frame);
        
        //start the method
        frame.start();
    }
    
    private void setParameters(VStackFrame frame, ArrayList<VType> parameters){
        int index = 0;
        for(int i = 0; i < parameters.size(); i++){
            switch(parameters.get(i).descriptor.charAt(0)){
                case 'B':
                    ByteType bt = (ByteType) parameters.get(i);
                    frame.storeByte(index, bt.getValue());
                    index++;
                    break;
                case 'C':
                    CharType ct = (CharType) parameters.get(i);
                    frame.storeChar(index, ct.getValue());
                    index++;
                    break;
                case 'D':
                    DoubleType dt = (DoubleType) parameters.get(i);
                    frame.storeDouble(index, dt.getValue());
                    index = index + 2;
                    break;
                case 'F':
                    FloatType ft = (FloatType) parameters.get(i);
                    frame.storeFloat(index, ft.getValue());
                    index++;
                    break;
                case 'I':
                    IntType it = (IntType) parameters.get(i);
                    frame.storeInt(index, it.getValue());
                    index++;
                    break;
                case 'J':
                    LongType lt = (LongType) parameters.get(i);
                    frame.storeLong(index, lt.getValue());
                    index = index + 2;
                    break;
                case 'L':
                    ReferenceType rt = (ReferenceType) parameters.get(i);
                    frame.storeReference(index, rt.getValue());
                    index++;
                    break;
                case 'S':
                    ShortType st = (ShortType) parameters.get(i);
                    frame.storeShort(index, st.getValue());
                    index++;
                    break;
                case 'Z':
                    BooleanType booleanType= (BooleanType) parameters.get(i);
                    frame.storeBoolean(index, booleanType.getValue());
                    index++;
                    break;
            }
        }
    }
}
