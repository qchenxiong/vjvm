/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Viload extends VInstruction{
    byte vindex;

    public Viload(byte byte1){
        this.vindex = byte1;
    }

    @Override
    public void execute(VStackFrame frame) {
        int i = frame.loadInt(vindex);
        frame.pushOperandInt(i);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 2;
    }
    
    
}
