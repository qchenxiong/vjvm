package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class ConstantInterfaceMethodrefInfo extends CPInfo{

    short class_index;
    short name_and_type_index;
    
    public ConstantInterfaceMethodrefInfo(byte tag, DataInputStream stream) throws IOException{
        this.tag = tag;
        class_index = stream.readShort();
        name_and_type_index = stream.readShort();
    }

    @Override
    public int getSize() {
        return 5;
    }
}
