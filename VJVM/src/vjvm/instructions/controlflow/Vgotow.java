/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.controlflow;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vgotow extends VInstruction{
    byte branchbyte1, branchbyte2, branchbyte3, branchbyte4;

    public Vgotow(byte byte1, byte byte2, byte byte3, byte byte4){
        this.branchbyte1 = byte1;
        this.branchbyte2 = byte2;
        this.branchbyte3 = byte3;
        this.branchbyte4 = byte4;
    }

    @Override
    public void execute(VStackFrame frame) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getByteSize() {
        return 5;
    }
    
}
