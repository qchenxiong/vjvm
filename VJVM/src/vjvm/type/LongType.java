/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.type;

/**
 *
 * @author vin
 */
public class LongType extends PrimitiveType{
    private long value;
    
    public LongType(){
        this.descriptor = "J";
    }
    
    public void setValue(long l){
        this.value = l;
    }
    
    public long getValue(){
        return value;
    }
}
