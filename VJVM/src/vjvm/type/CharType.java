/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.type;

/**
 *
 * @author vin
 */
public class CharType extends PrimitiveType{
    private char value;
    
    public CharType(){
        this.descriptor = "C";
    }
    
    public void setValue(char c){
        this.value = c;
    }
    
    public char getValue(){
        return value;
    }
}
