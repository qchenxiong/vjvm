/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.type;

/**
 *
 * @author vin
 */
public class ByteType extends PrimitiveType{
    byte value;
    
    public ByteType(){
        this.descriptor = "B";
    }
    
    public void setValue(byte b){
        this.value = b;
    }
    
    public byte getValue(){
        return value;
    }
}
