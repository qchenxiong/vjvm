/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions;

import vjvm.instructions.typeconversion.Vi2f;
import vjvm.instructions.typeconversion.Vd2f;
import vjvm.instructions.typeconversion.Vf2l;
import vjvm.instructions.typeconversion.Vf2d;
import vjvm.instructions.typeconversion.Vd2i;
import vjvm.instructions.typeconversion.Vi2s;
import vjvm.instructions.typeconversion.Vl2d;
import vjvm.instructions.typeconversion.Vl2f;
import vjvm.instructions.typeconversion.Vl2i;
import vjvm.instructions.typeconversion.Vf2i;
import vjvm.instructions.typeconversion.Vi2d;
import vjvm.instructions.typeconversion.Vi2l;
import vjvm.instructions.typeconversion.Vi2b;
import vjvm.instructions.stackoperation.Vwaload;
import vjvm.instructions.stackoperation.Vdup2_x2;
import vjvm.instructions.stackoperation.Vastore;
import vjvm.instructions.stackoperation.Vdload;
import vjvm.instructions.stackoperation.Vnop;
import vjvm.instructions.stackoperation.Vaload_n;
import vjvm.instructions.stackoperation.Vlload;
import vjvm.instructions.stackoperation.Vaconst_null;
import vjvm.instructions.stackoperation.Vlstore;
import vjvm.instructions.stackoperation.Vastore_n;
import vjvm.instructions.stackoperation.Vlload_n;
import vjvm.instructions.stackoperation.Viconst;
import vjvm.instructions.stackoperation.Vwdstore;
import vjvm.instructions.stackoperation.Vwastore;
import vjvm.instructions.stackoperation.Vfload;
import vjvm.instructions.stackoperation.Vdconst;
import vjvm.instructions.stackoperation.Vfstore_n;
import vjvm.instructions.stackoperation.Vbipush;
import vjvm.instructions.stackoperation.Vwfstore;
import vjvm.instructions.stackoperation.Vwistore;
import vjvm.instructions.stackoperation.Viload_n;
import vjvm.instructions.stackoperation.Vlstore_n;
import vjvm.instructions.stackoperation.Vdup_x1;
import vjvm.instructions.stackoperation.Vdup;
import vjvm.instructions.stackoperation.Vfstore;
import vjvm.instructions.stackoperation.Vistore_n;
import vjvm.instructions.stackoperation.Vdup_x2;
import vjvm.instructions.stackoperation.Vldc_w;
import vjvm.instructions.stackoperation.Vwfload;
import vjvm.instructions.stackoperation.Vwdload;
import vjvm.instructions.stackoperation.Vfconst;
import vjvm.instructions.stackoperation.Vldc2_w;
import vjvm.instructions.stackoperation.Vpop2;
import vjvm.instructions.stackoperation.Vdload_n;
import vjvm.instructions.stackoperation.Vlconst;
import vjvm.instructions.stackoperation.Vfload_n;
import vjvm.instructions.stackoperation.Vwiload;
import vjvm.instructions.stackoperation.Vaload;
import vjvm.instructions.stackoperation.Vldc;
import vjvm.instructions.stackoperation.Viload;
import vjvm.instructions.stackoperation.Vpop;
import vjvm.instructions.stackoperation.Vwlstore;
import vjvm.instructions.stackoperation.Vsipush;
import vjvm.instructions.stackoperation.Vistore;
import vjvm.instructions.stackoperation.Vwlload;
import vjvm.instructions.stackoperation.Vdstore_n;
import vjvm.instructions.stackoperation.Vdstore;
import vjvm.instructions.stackoperation.Vswap;
import vjvm.instructions.stackoperation.Vdup2_x1;
import vjvm.instructions.objectsandarrays.Vlaload;
import vjvm.instructions.objectsandarrays.Vsastore;
import vjvm.instructions.objectsandarrays.Vinstanceof;
import vjvm.instructions.objectsandarrays.Vdastore;
import vjvm.instructions.objectsandarrays.Vbastore;
import vjvm.instructions.objectsandarrays.Vaastore;
import vjvm.instructions.objectsandarrays.Vdaload;
import vjvm.instructions.objectsandarrays.Vnew;
import vjvm.instructions.objectsandarrays.Vgetfield;
import vjvm.instructions.objectsandarrays.Vgetstatic;
import vjvm.instructions.objectsandarrays.Vcaload;
import vjvm.instructions.objectsandarrays.Vlastore;
import vjvm.instructions.objectsandarrays.Vfastore;
import vjvm.instructions.objectsandarrays.Vcheckcast;
import vjvm.instructions.objectsandarrays.Varraylength;
import vjvm.instructions.objectsandarrays.Vaaload;
import vjvm.instructions.objectsandarrays.Viastore;
import vjvm.instructions.objectsandarrays.Vmultianewarray;
import vjvm.instructions.objectsandarrays.Vsaload;
import vjvm.instructions.objectsandarrays.Vputstatic;
import vjvm.instructions.objectsandarrays.Vputfield;
import vjvm.instructions.objectsandarrays.Vnewarray;
import vjvm.instructions.objectsandarrays.Viaload;
import vjvm.instructions.objectsandarrays.Vanewarray;
import vjvm.instructions.objectsandarrays.Vcastore;
import vjvm.instructions.objectsandarrays.Vbaload;
import vjvm.instructions.objectsandarrays.Vfaload;
import vjvm.instructions.mthods.Vinvokeinterface;
import vjvm.instructions.mthods.Vlreturn;
import vjvm.instructions.mthods.Vinvokevirtual;
import vjvm.instructions.mthods.Vinvokespecial;
import vjvm.instructions.mthods.Vareturn;
import vjvm.instructions.mthods.Vfreturn;
import vjvm.instructions.mthods.Vdreturn;
import vjvm.instructions.mthods.V_return;
import vjvm.instructions.mthods.Vinvokestatic;
import vjvm.instructions.mthods.Vireturn;
import vjvm.instructions.logicoperation.Vishl;
import vjvm.instructions.logicoperation.Vishr;
import vjvm.instructions.logicoperation.Viushr;
import vjvm.instructions.logicoperation.Vlshl;
import vjvm.instructions.logicoperation.Vlshr;
import vjvm.instructions.logicoperation.Vioxr;
import vjvm.instructions.logicoperation.Vlor;
import vjvm.instructions.logicoperation.Vloxr;
import vjvm.instructions.logicoperation.Viand;
import vjvm.instructions.logicoperation.Vland;
import vjvm.instructions.logicoperation.Vlushr;
import vjvm.instructions.logicoperation.Vior;
import vjvm.instructions.integerarithmetic.Vlrem;
import vjvm.instructions.integerarithmetic.Visub;
import vjvm.instructions.integerarithmetic.Vimul;
import vjvm.instructions.integerarithmetic.Viinc;
import vjvm.instructions.integerarithmetic.Virem;
import vjvm.instructions.integerarithmetic.Vldiv;
import vjvm.instructions.integerarithmetic.Vladd;
import vjvm.instructions.integerarithmetic.Vineg;
import vjvm.instructions.integerarithmetic.Vidiv;
import vjvm.instructions.integerarithmetic.Vlsub;
import vjvm.instructions.integerarithmetic.Vlneg;
import vjvm.instructions.integerarithmetic.Viadd;
import vjvm.instructions.integerarithmetic.Vlmul;
import vjvm.instructions.integerarithmetic.Vwiinc;
import vjvm.instructions.floatpointarithmetic.Vfsub;
import vjvm.instructions.floatpointarithmetic.Vfadd;
import vjvm.instructions.floatpointarithmetic.Vdmul;
import vjvm.instructions.floatpointarithmetic.Vfneg;
import vjvm.instructions.floatpointarithmetic.Vdneg;
import vjvm.instructions.floatpointarithmetic.Vfrem;
import vjvm.instructions.floatpointarithmetic.Vdrem;
import vjvm.instructions.floatpointarithmetic.Vdsub;
import vjvm.instructions.floatpointarithmetic.Vddiv;
import vjvm.instructions.floatpointarithmetic.Vfdiv;
import vjvm.instructions.floatpointarithmetic.Vfmul;
import vjvm.instructions.floatpointarithmetic.Vdadd;
import vjvm.instructions.controlflow.Vif_icmplt;
import vjvm.instructions.controlflow.Vifle;
import vjvm.instructions.controlflow.Vifne;
import vjvm.instructions.controlflow.Viflt;
import vjvm.instructions.controlflow.Vifge;
import vjvm.instructions.controlflow.Vifgt;
import vjvm.instructions.controlflow.Vfcmpl;
import vjvm.instructions.controlflow.Vgotow;
import vjvm.instructions.controlflow.Vfcmpg;
import vjvm.instructions.controlflow.Vlcmp;
import vjvm.instructions.controlflow.Vif_icmple;
import vjvm.instructions.controlflow.Vifnonnull;
import vjvm.instructions.controlflow.Vif_icmpge;
import vjvm.instructions.controlflow.Vdcmpg;
import vjvm.instructions.controlflow.Vdcmpl;
import vjvm.instructions.controlflow.Vifnull;
import vjvm.instructions.controlflow.Vif_icmpne;
import vjvm.instructions.controlflow.Vifeq;
import vjvm.instructions.controlflow.Vif_acmpeq;
import vjvm.instructions.controlflow.Vif_icmpgt;
import vjvm.instructions.controlflow.Vif_icmpeq;
import vjvm.instructions.controlflow.Vif_acmpne;
import vjvm.instructions.controlflow.Vgoto;
import java.util.ArrayList;

/**
 *
 * @author vin
 */
public class VInstructionFactory {
    
    static boolean isWide = false;
    
    public static ArrayList<VInstruction> getInstructions(byte codes[]){
        ArrayList<VInstruction> instructions = new ArrayList<VInstruction>();
        for(int i = 0; i < codes.length; i++){
            VInstruction instruction = null;
            int opcode = codes[i] & 0xff;
            switch(opcode){
                case OpcodeConstants.nop:
                    instruction = new Vnop();
                    break;
                case OpcodeConstants.aconst_null:
                    instruction = new Vaconst_null();
                    break;
                case OpcodeConstants.iconst_m1:
                    instruction = new Viconst(-1);
                    break;
                case OpcodeConstants.iconst_0:
                    instruction = new Viconst(0);
                    break;
                case OpcodeConstants.iconst_1:
                    instruction = new Viconst(1);
                    break;
                case OpcodeConstants.iconst_2:
                    instruction = new Viconst(2);
                    break;
                case OpcodeConstants.iconst_3:
                    instruction = new Viconst(3);
                    break;
                case OpcodeConstants.iconst_4:
                    instruction = new Viconst(4);
                    break;
                case OpcodeConstants.iconst_5:
                    instruction = new Viconst(5);
                    break;
                case OpcodeConstants.lconst_0:
                    instruction = new Vlconst(0);
                    break;
                case OpcodeConstants.lconst_1:
                    instruction = new Vlconst(1);  
                    break;
                case OpcodeConstants.fconst_0:
                    instruction = new Vfconst(0.0f);
                    break;
                case OpcodeConstants.fconst_1:
                    instruction = new Vfconst(1.0f);
                    break;
                case OpcodeConstants.fconst_2:
                    instruction = new Vfconst(2.0f);
                    break;
                case OpcodeConstants.dconst_0:
                    instruction = new Vdconst(0.0);
                    break;
                case OpcodeConstants.dconst_1:
                    instruction = new Vdconst(1.0);
                    break;
                case OpcodeConstants.bipush:
                    instruction = new Vbipush(codes[i+1]);
                    i++;
                    break;
                case OpcodeConstants.sipush:
                    instruction = new Vsipush(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.ldc:
                    instruction = new Vldc(codes[i+1]);
                    i++;
                    break;
                case OpcodeConstants.ldc_w:
                    instruction = new Vldc_w(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.ldc2_w:
                    instruction = new Vldc2_w(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.pop:
                    instruction = new Vpop();
                    break;
                case OpcodeConstants.pop2:
                    instruction = new Vpop2();
                    break;
                case OpcodeConstants.dup:
                    instruction = new Vdup();
                    break;
                case OpcodeConstants.dup_x1:
                    instruction = new Vdup_x1();
                    break;
                case OpcodeConstants.dup_x2:
                    instruction = new Vdup_x2();
                    break;
                case OpcodeConstants.dup2_x1:
                    instruction = new Vdup2_x1();
                    break;
                case OpcodeConstants.dup2_x2:
                    instruction = new Vdup2_x2();
                    break;
                case OpcodeConstants.swap:
                    instruction = new Vswap();
                    break;
                case OpcodeConstants.iload:
                    if(!isWide){
                       instruction = new Viload(codes[i+1]);
                       i++; 
                    }else{
                       instruction = new Vwiload(codes[i+1],codes[i+2]);
                       i = i + 2;
                       isWide = false;
                    }
                    break;
                case OpcodeConstants.iload_0:
                    instruction = new Viload_n(0);
                    break;
                case OpcodeConstants.iload_1:
                    instruction = new Viload_n(1);
                    break;
                case OpcodeConstants.iload_2:
                    instruction = new Viload_n(2);
                    break;
                case OpcodeConstants.iload_3:
                    instruction = new Viload_n(3);
                    break;
                case OpcodeConstants.fload:
                    if(!isWide){
                       instruction = new Vfload(codes[i+1]);
                       i++; 
                    }else{
                       instruction = new Vwfload(codes[i+1],codes[i+2]);
                       i = i + 2;
                       isWide = false;
                    }
                    break;
                case OpcodeConstants.fload_0:
                    instruction = new Vfload_n(0);
                    break;
                case OpcodeConstants.fload_1:
                    instruction = new Vfload_n(1);
                    break;
                case OpcodeConstants.fload_2:
                    instruction = new Vfload_n(2);
                    break;
                case OpcodeConstants.fload_3:
                    instruction = new Vfload_n(3);
                    break;
                case OpcodeConstants.lload:
                    if(!isWide){
                        instruction = new Vlload(codes[i+1]);
                        i++;
                    }else{
                        instruction = new Vwlload(codes[i+1],codes[i+2]);
                        i = i + 2;
                        isWide = false;
                    }
                    break;
                case OpcodeConstants.lload_0:
                    instruction = new Vlload_n(0);
                    break;
                case OpcodeConstants.lload_1:
                    instruction = new Vlload_n(1);
                    break;
                case OpcodeConstants.lload_2:
                    instruction = new Vlload_n(2);
                    break;
                case OpcodeConstants.lload_3:
                    instruction = new Vlload_n(3);
                    break;
                case OpcodeConstants.dload:
                    if(!isWide){
                        instruction = new Vdload(codes[i+1]);
                        i++;    
                    }else{
                        instruction = new Vwdload(codes[i+1],codes[i+2]);
                        i = i + 2;
                        isWide = false;
                    }
                    break;
                case OpcodeConstants.dload_0:
                    instruction = new Vdload_n(0);
                    break;
                case OpcodeConstants.dload_1:
                    instruction = new Vdload_n(1);
                    break;
                case OpcodeConstants.dload_2:
                    instruction = new Vdload_n(2);
                    break;
                case OpcodeConstants.dload_3:
                    instruction = new Vdload_n(3);
                    break;
                case OpcodeConstants.aload:
                    if(!isWide){
                        instruction = new Vaload(codes[i+1]);
                        i++;
                    }else{
                        instruction = new Vwaload(codes[i+1],codes[i+2]);
                        i = i + 2;
                        isWide = false;
                    }
                    break;
                case OpcodeConstants.aload_0:
                    instruction = new Vaload_n(0);
                    break;
                case OpcodeConstants.aload_1:
                    instruction = new Vaload_n(1);
                    break;
                case OpcodeConstants.aload_2:
                    instruction = new Vaload_n(2);
                    break;
                case OpcodeConstants.aload_3:
                    instruction = new Vaload_n(3);
                    break;
                case OpcodeConstants.istore:
                    if(!isWide){
                        instruction = new Vistore(codes[i+1]);
                        i++;
                    }else{
                        instruction = new Vwistore(codes[i+1],codes[i+2]);
                        i = i + 2;
                        isWide = false;
                    }
                    break;
                case OpcodeConstants.istore_0:
                    instruction = new Vistore_n(0);
                    break;
                case OpcodeConstants.istore_1:
                    instruction = new Vistore_n(1);
                    break;
                case OpcodeConstants.istore_2:
                    instruction = new Vistore_n(2);
                    break;
                case OpcodeConstants.istore_3:
                    instruction = new Vistore_n(3);
                    break;
                case OpcodeConstants.fstore:
                    if(!isWide){
                        instruction = new Vfstore(codes[i+1]);
                        i++;
                    }else{
                        instruction = new Vwfstore(codes[i+1],codes[i+2]);
                        i = i + 2;
                        isWide = false;
                    }
                    break;
                case OpcodeConstants.fstore_0:
                    instruction = new Vfstore_n(0);
                    break;
                case OpcodeConstants.fstore_1:
                    instruction = new Vfstore_n(1);
                    break;
                case OpcodeConstants.fstore_2:
                    instruction = new Vfstore_n(2);
                    break;
                case OpcodeConstants.fstore_3:
                    instruction = new Vfstore_n(3);
                    break;
                case OpcodeConstants.lstore:
                    if(!isWide){
                        instruction = new Vlstore(codes[i+1]);
                        i++;
                    }else{
                        instruction = new Vwlstore(codes[i+1],codes[i+2]);
                        i =  i + 2;
                        isWide = false;
                    }
                    break;
                case OpcodeConstants.lstore_0:
                    instruction = new Vlstore_n(0);
                    break;
                case OpcodeConstants.lstore_1:
                    instruction = new Vlstore_n(1);
                    break;
                case OpcodeConstants.lstore_2:
                    instruction = new Vlstore_n(2);
                    break;
                case OpcodeConstants.lstore_3:
                    instruction = new Vlstore_n(3);
                    break;         
                case OpcodeConstants.dstore:
                    if(!isWide){
                        instruction = new Vdstore(codes[i+1]);
                        i++;
                    }else{
                        instruction = new Vwdstore(codes[i+1],codes[i+2]);
                        i =  i + 2;
                        isWide = false;
                    }
                    break;
                case OpcodeConstants.dstore_0:
                    instruction = new Vdstore_n(0);
                    break;
                case OpcodeConstants.dstore_1:
                    instruction = new Vdstore_n(1);
                    break;
                case OpcodeConstants.dstore_2:
                    instruction = new Vdstore_n(2);
                    break;
                case OpcodeConstants.dstore_3:
                    instruction = new Vdstore_n(3);
                    break;
                case OpcodeConstants.astore:
                    if(!isWide){
                        instruction = new Vastore(codes[i+1]);
                        i++;
                    }else{
                        instruction = new Vwastore(codes[i+1],codes[i+2]);
                        i = i + 2;
                        isWide = false;
                    }
                    break;
                case OpcodeConstants.astore_0:
                    instruction = new Vastore_n(0);
                    break;
                case OpcodeConstants.astore_1:
                    instruction = new Vastore_n(1);
                    break;
                case OpcodeConstants.astore_2:
                    instruction = new Vastore_n(2);
                    break;
                case OpcodeConstants.astore_3:
                    instruction = new Vastore_n(3);
                    break;
                case OpcodeConstants.i2l:
                    instruction = new Vi2l();
                    break;
                case OpcodeConstants.i2f:
                    instruction = new Vi2f();
                    break;
                case OpcodeConstants.i2d:
                    instruction = new Vi2d();
                    break;
                case OpcodeConstants.l2i:
                    instruction = new Vl2i();
                    break;
                case OpcodeConstants.l2f:
                    instruction = new Vl2f();
                    break;
                case OpcodeConstants.l2d:
                    instruction = new Vl2d();
                    break;
                case OpcodeConstants.f2i:
                    instruction = new Vf2i();
                    break;
                case OpcodeConstants.f2l:
                    instruction = new Vf2l();
                    break;
                case OpcodeConstants.f2d:
                    instruction = new Vf2d();
                    break;
                case OpcodeConstants.d2i:
                    instruction = new Vd2i();
                    break;
                case OpcodeConstants.d2l:
                    instruction = new Vd2f();
                    break;
                case OpcodeConstants.i2b:
                    instruction = new Vi2b();
                    break;
                case OpcodeConstants.i2c:
                    instruction = new Vi2s();
                    break;
                case OpcodeConstants.iadd:
                    instruction = new Viadd();
                    break;
                case OpcodeConstants.ladd:
                    instruction = new Vladd();
                    break;
                case OpcodeConstants.iinc:
                    if(!isWide){
                        instruction = new Viinc(codes[i+1],codes[i+2]);
                        i = i + 2;
                    }else{
                        instruction = new Vwiinc(codes[i+1],codes[i+2],codes[i+3],codes[i+4]);
                        i = i + 4;
                        isWide = false;
                    }
                    break;
                case OpcodeConstants.isub:
                    instruction = new Visub();
                    break;
                case OpcodeConstants.lsub:
                    instruction = new Vlsub();
                    break;
                case OpcodeConstants.imul:
                    instruction = new Vimul();
                    break;
                case OpcodeConstants.lmul:
                    instruction = new Vlmul();
                    break;
                case OpcodeConstants.idiv:
                    instruction = new Vidiv();
                    break;
                case OpcodeConstants.ldiv:
                    instruction = new Vldiv();
                    break;
                case OpcodeConstants.irem:
                    instruction = new Virem();
                    break;
                case OpcodeConstants.lrem:
                    instruction = new Vlrem();
                    break;
                case OpcodeConstants.ineg:
                    instruction = new Vineg();
                    break;
                case OpcodeConstants.lneg:
                    instruction = new Vlneg();
                    break;
                case OpcodeConstants.fadd:
                    instruction = new Vfadd();
                    break;
                case OpcodeConstants.dadd:
                    instruction = new Vdadd();
                    break;
                case OpcodeConstants.fsub:
                    instruction = new Vfsub();
                    break;
                case OpcodeConstants.dsub:
                    instruction = new Vdsub();
                    break;
                case OpcodeConstants.fmul:
                    instruction = new Vfmul();
                    break;
                case OpcodeConstants.dmul:
                    instruction = new Vdmul();
                    break;
                case OpcodeConstants.fdiv:
                    instruction = new Vfdiv();
                    break;
                case OpcodeConstants.ddiv:
                    instruction = new Vddiv();
                    break;
                case OpcodeConstants.frem:
                    instruction = new Vfrem();
                    break;
                case OpcodeConstants.drem:
                    instruction = new Vdrem();
                    break;
                case OpcodeConstants.fneg:
                    instruction = new Vfneg();
                    break;
                case OpcodeConstants.dneg:
                    instruction = new Vdneg();
                    break;
                case OpcodeConstants.ishl:
                    instruction = new Vishl();
                    break;
                case OpcodeConstants.ishr:
                    instruction = new Vishr();
                    break;
                case OpcodeConstants.iushr:
                    instruction = new Viushr();
                    break;
                case OpcodeConstants.lshl:
                    instruction = new Vlshl();
                    break;
                case OpcodeConstants.lshr:
                    instruction = new Vlshr();
                    break;
                case OpcodeConstants.lushr:
                    instruction = new Vlushr();
                    break;
                case OpcodeConstants.iand:
                    instruction = new Viand();
                    break;
                case OpcodeConstants.ior:
                    instruction = new Vior();
                    break;
                case OpcodeConstants.ioxr:
                    instruction = new Vioxr();
                    break;
                case OpcodeConstants.land:
                    instruction = new Vland();
                    break;
                case OpcodeConstants.lor:
                    instruction = new Vlor();
                    break;
                case OpcodeConstants.loxr:
                    instruction = new Vloxr();
                    break;
                case OpcodeConstants._new:
                    instruction = new Vnew(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.getfield:
                    instruction = new Vgetfield(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.putfield:
                    instruction = new Vputfield(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.getstatic:
                    instruction = new Vgetstatic(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.putstatic:
                    instruction = new Vputstatic(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.checkcast:
                    instruction = new Vcheckcast(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants._instanceof:
                    instruction = new Vinstanceof(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.newarray:
                    instruction = new Vnewarray(codes[i+1]);
                    i++;
                    break;
                case OpcodeConstants.anewarray:
                    instruction = new Vanewarray(codes[i+1], codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.multianewarray:
                    instruction = new Vmultianewarray(codes[i+1],codes[i+2],codes[i+3]);
                    i = i + 3;
                    break;
                case OpcodeConstants.arraylength:
                    instruction = new Varraylength();
                    break;
                case OpcodeConstants.baload:
                    instruction = new Vbaload();
                    break;
                case OpcodeConstants.caload:
                    instruction = new Vcaload();
                    break;
                case OpcodeConstants.saload:
                    instruction = new Vsaload();
                    break;    
                case OpcodeConstants.iaload:
                    instruction = new Viaload();
                    break;  
                case OpcodeConstants.laload:
                    instruction = new Vlaload();
                    break;
                case OpcodeConstants.faload:
                    instruction = new Vfaload();
                    break;
                case OpcodeConstants.daload:
                    instruction = new Vdaload();
                    break;
                case OpcodeConstants.aaload:
                    instruction = new Vaaload();
                    break;
                case OpcodeConstants.bastore:
                    instruction = new Vbastore();
                    break;
                case OpcodeConstants.castore:
                    instruction = new Vcastore();
                    break;
                case OpcodeConstants.sastore:
                    instruction = new Vsastore();
                    break;    
                case OpcodeConstants.iastore:
                    instruction = new Viastore();
                    break;  
                case OpcodeConstants.lastore:
                    instruction = new Vlastore();
                    break;
                case OpcodeConstants.fastore:
                    instruction = new Vfastore();
                    break;
                case OpcodeConstants.dastore:
                    instruction = new Vdastore();
                    break;
                case OpcodeConstants.aastore:
                    instruction = new Vaastore();
                    break;
                case OpcodeConstants.ifeq:
                    instruction = new Vifeq(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.ifne:
                    instruction = new Vifne(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.iflt:
                    instruction = new Viflt(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.ifge:
                    instruction = new Vifge(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.ifgt:
                    instruction = new Vifgt(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.ifle:
                    instruction = new Vifle(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.if_icmpeq:
                    instruction = new Vif_icmpeq(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.if_icmpne:
                    instruction = new Vif_icmpne(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.if_icmplt:
                    instruction = new Vif_icmplt(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.if_icmpge:
                    instruction = new Vif_icmpge(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.if_icmpgt:
                    instruction = new Vif_icmpgt(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.if_icmple:
                    instruction = new Vif_icmple(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.lcmp:
                    instruction = new Vlcmp();
                    break;
                case OpcodeConstants.fcmpg:
                    instruction = new Vfcmpg();
                    break;
                case OpcodeConstants.fcmpl:
                    instruction = new Vfcmpl();
                    break;
                case OpcodeConstants.dcmpg:
                    instruction = new Vdcmpg();
                    break;
                case OpcodeConstants.dcmpl:
                    instruction = new Vdcmpl();
                    break;
                case OpcodeConstants.ifnull:
                    instruction = new Vifnull(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.ifnonnull:
                    instruction = new Vifnonnull(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.if_acmpeq:
                    instruction = new Vif_acmpeq(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.if_acmpne:
                    instruction = new Vif_acmpne(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants._goto:
                    instruction = new Vgoto(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.goto_w:
                    instruction = new Vgotow(codes[i+1],codes[i+2],codes[i+3],codes[i+4]);
                    i = i + 4;
                    break;
                case OpcodeConstants.lookupswitch:
                    System.err.println("not support lookupswitch yet");
                    System.exit(0);
                    break;
                case OpcodeConstants.tableswitch:
                    System.err.println("not support tableswitch yet");
                    System.exit(0);
                    break;
                case OpcodeConstants.athrow:/* TODO ret has a wide*/
                case OpcodeConstants.jsr:
                case OpcodeConstants.jsr_w:
                case OpcodeConstants.ret:
                    System.err.println("not support exception yet");
                    System.exit(0);
                    break;
                case OpcodeConstants.invokevirtual:
                    instruction = new Vinvokevirtual(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.invokespecial:
                    instruction = new Vinvokespecial(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.invokestatic:
                    instruction = new Vinvokestatic(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.invokeinterface:
                    instruction = new Vinvokeinterface(codes[i+1],codes[i+2]);
                    i = i + 2;
                    break;
                case OpcodeConstants.ireturn:
                    instruction = new Vireturn();
                    break;
                case OpcodeConstants.lreturn:
                    instruction = new Vlreturn();
                    break;
                case OpcodeConstants.freturn:
                    instruction = new Vfreturn();
                    break;  
                case OpcodeConstants.dreturn:
                    instruction = new Vdreturn();
                    break;
                case OpcodeConstants.areturn:
                    instruction = new Vareturn();
                    break;
                case OpcodeConstants._return:
                    instruction = new V_return();
                    break;
                case OpcodeConstants.monitorenter:
                case OpcodeConstants.monitorexit:
                    System.err.println("not support multi thread yet");
                    System.exit(0);
                    break;
                case OpcodeConstants.wide:
                    System.err.println("not support wide yet");
                    System.exit(0);
                    break;
            }
            instructions.add(instruction);
        }
        return instructions;
    }
}
