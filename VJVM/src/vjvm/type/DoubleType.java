/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.type;

/**
 *
 * @author vin
 */
public class DoubleType extends PrimitiveType{
    private double value;
    
    public DoubleType(){
        this.descriptor = "D";
    }
    
    public void setValue(double d){
        this.value = d;
    }
    
    public double getValue(){
        return value;
    }
}
