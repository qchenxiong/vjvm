package vjvm.classfile;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import vjvm.runtime.ClassHeap;
import vjvm.runtime.ObjectHeap;
import vjvm.runtime.VObject;
import vjvm.type.BooleanType;
import vjvm.type.ByteType;
import vjvm.type.CharType;
import vjvm.type.DoubleType;
import vjvm.type.FloatType;
import vjvm.type.IntType;
import vjvm.type.LongType;
import vjvm.type.ReferenceType;
import vjvm.type.ShortType;
import vjvm.type.VType;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class JavaClass {
    public static final int ACC_PUBLIC = 0x0001;
    public static final int ACC_FINAL = 0x0010;
    public static final int ACC_SUPER = 0x0020;
    public static final int ACC_INTERFACE = 0x0200;
    public static final int ACC_ABSTRACT = 0x0400;
    
    public static final int ACC_PRIVATE = 0x0002;
    public static final int ACC_PROTECTED = 0x0004;
    public static final int ACC_STATIC = 0x0008;
    public static final int ACC_SYNCHRONIZED = 0x0020;
    public static final int ACC_NATIVE = 0x0100;
    public static final int ACC_STRICT = 0x0800;
    
    int magic;
    short minor_version;
    short major_version;
    short constant_pool_count;
    CPInfo constant_pool[];
    short access_flags;
    short this_class;
    short super_class;
    short interfaces_count;
    short interfaces[];
    short fields_count;
    FieldInfo fields[];
    short methods_count;
    MethodInfo methods[];
    short attributes_count;
    AttributeInfo attributes[];
    
    private String filepath;
    private File classFile;
    private long filesize;
    private long bytesRead;
    private DataInputStream stream;
    
    public JavaClass(String filepath) throws IOException{
        this.filepath = filepath;
        classFile = new File(filepath);
        filesize = classFile.length();
        bytesRead = 0;
        try {
            stream = new DataInputStream(new FileInputStream(classFile));
            parseClass();
        } catch (FileNotFoundException ex) {
            System.err.println("file not found at: "+filepath);
            System.exit(0);
        }
    }
    
    private boolean parseClass() throws IOException{
        magic = stream.readInt(); bytesRead += 4;
        minor_version = stream.readShort(); bytesRead += 2;
        major_version = stream.readShort(); bytesRead += 2;
        
        constant_pool_count = stream.readShort(); bytesRead += 2;
        if(constant_pool_count > 0){
            parseConstantPool();
        }
        
        access_flags = stream.readShort(); bytesRead += 2;
        this_class = stream.readShort(); bytesRead += 2;
        super_class = stream.readShort(); bytesRead += 2;
        
        interfaces_count = stream.readShort(); bytesRead += 2;
        if(interfaces_count > 0){
            parseInterfaces();
        }
        
        fields_count = stream.readShort(); bytesRead += 2;
        if(fields_count > 0){
            parseFields();
        }
        
        methods_count = stream.readShort(); bytesRead += 2;
        if(methods_count > 0){
            parseMethods();
        }
        
        attributes_count = stream.readShort(); bytesRead += 2;
        if(attributes_count > 0){
            parseAttributes();
        }
        
        return true;
    }
    
    private boolean parseConstantPool() throws IOException{
        constant_pool = new CPInfo[constant_pool_count - 1];
        for(int i = 0; i < constant_pool_count - 1; i++){
            byte tag = stream.readByte();
            switch(tag){
                case CPInfo.CONSTANT_Class:
                    constant_pool[i] = new ConstantClassInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_Fieldref:
                    constant_pool[i] = new ConstantFieldrefInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_Methodref:
                    constant_pool[i] = new ConstantMethodrefInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_InterfaceMethodref:
                    constant_pool[i] = new ConstantInterfaceMethodrefInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_String:
                    constant_pool[i] = new ConstantStringInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_Integer:
                    constant_pool[i] = new ConstantIntegerInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_Float:
                    constant_pool[i] = new ConstantFloatInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_Long:
                    constant_pool[i] = new ConstantLongInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_Double:
                    constant_pool[i] = new ConstantDoubleInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_NameAndType:
                    constant_pool[i] = new ConstantNameAndTypeInfo(tag, stream);
                    break;
                case CPInfo.CONSTANT_Utf8:
                    constant_pool[i] = new ConstantUtf8Info(tag, stream);
                    break;
                default:
                    System.err.println("Not write constant pool tag: "+tag);
                    return false;
            }
            bytesRead += constant_pool[i].getSize(); 
            if(tag == CPInfo.CONSTANT_Double || tag == CPInfo.CONSTANT_Long){
                i++;
            }
        }
        return true;
    }
    
    public String getStringFromCP(short name_index){
        if(name_index < 0 || name_index > constant_pool_count - 2 || 
                constant_pool[name_index-1].tag != CPInfo.CONSTANT_Utf8){
            return null;
        }else{
            return constant_pool[name_index-1].toString();
        }
    }
    
    public String getName(){
        if(constant_pool[this_class-1].tag != CPInfo.CONSTANT_Class){
            return null;
        }else{
            short name_index = ((ConstantClassInfo)constant_pool[this_class-1]).name_index;
            return getStringFromCP(name_index);
        }
    }
    
    public String getSuperClassName(){
        if(constant_pool[super_class-1].tag != CPInfo.CONSTANT_Class){
            return null;
        }else{
            short name_index = ((ConstantClassInfo)constant_pool[super_class-1]).name_index;
            return getStringFromCP(name_index);
        }
    }
    
    public int createInstance(){
        VType types[] = new VType[fields_count];
        for(int i = 0; i < fields_count; i++){
            String descriptor = getStringFromCP(fields[i].descriptor_index);
            switch(descriptor.charAt(0)){
                case 'B':
                    types[i] = new ByteType();
                    break;
                case 'C':
                    types[i] = new CharType();
                    break;
                case 'D':
                    types[i] = new DoubleType();
                    break;
                case 'F':
                    types[i] = new FloatType();
                    break;
                case 'I':
                    types[i] = new IntType();
                    break;
                case 'J':
                    types[i] = new LongType();
                    break;
                case 'L':
                    types[i] = new ReferenceType();
                    //the class not loaded
                    int index = descriptor.lastIndexOf("/");
                    if(index == -1){
                        index = 0;
                    }
                    String className = descriptor.substring(index+1, descriptor.length()-1);
                    if(ClassHeap.getInstance().getClass(className) == null){
                        System.err.println("class not found: "+descriptor);
                        System.exit(0);
                    }
                    break;
                case 'S':
                    types[i] = new ShortType();
                    break;
                case 'Z':
                    types[i] = new BooleanType();
                    break;
                default:
                    System.err.println("Not support type: "+descriptor.charAt(0));
                    System.exit(0);
                    break;
            }
            types[i].descriptor = descriptor;
        }
        VObject vobj = new VObject(this,types);
        return ObjectHeap.getInstance().addObject(vobj);
    }
    
    private boolean parseInterfaces() throws IOException{
        interfaces = new short[interfaces_count];
        for(int i = 0; i < interfaces_count; i++){
            interfaces[i] = stream.readShort();
        }
        bytesRead += interfaces_count * 2;
        return true;
    }
    
    private boolean parseFields() throws IOException{
        fields = new FieldInfo[fields_count];
        for(int i = 0; i < fields_count; i++){
            fields[i] = new FieldInfo(stream,this);
            bytesRead += fields[i].getSize();
        }
        return true;
    }

    private boolean parseMethods() throws IOException{
        methods = new MethodInfo[methods_count];
        for(int i = 0; i < methods_count; i++){
            methods[i] = new MethodInfo(stream, this);
            bytesRead += methods[i].getSize();
        }
        return true;
    }
    
    private boolean parseAttributes() throws IOException{
        attributes = new AttributeInfo[attributes_count];
        //ignores these class attributes
        for(int i = 0; i < attributes_count; i++){
            stream.skipBytes(2); bytesRead += 2;
            int attribute_length = stream.readInt(); bytesRead += 4;
            stream.skipBytes(attribute_length); bytesRead += attribute_length;
        }
        return true;
    }

    public int getMethodIndex(String methodName){
        for(int i = 0; i < methods_count; i++){
            if(getStringFromCP(methods[i].name_index).equals(methodName)){
              return i;  
            }
        }
        return -1;
    }
    
    public byte[] getMethodCodes(int methodIndex){
        return methods[methodIndex].getCodeAttribute().code;
    }
    
    public short getMethodStackLength(int methodIndex){
        return methods[methodIndex].getCodeAttribute().max_stack;
    }
    
    public short getMethodMaxLocals(int methodIndex){
        return methods[methodIndex].getCodeAttribute().max_locals;
    }
    
    public String getClassNameByMethodRef(int methodRefIndex){
        if(constant_pool[methodRefIndex-1].tag == CPInfo.CONSTANT_Methodref){
            int classNameIndex = ((ConstantMethodrefInfo)constant_pool[methodRefIndex-1]).class_index;
            if(constant_pool[classNameIndex-1].tag == CPInfo.CONSTANT_Class){
                int nameIndex = ((ConstantClassInfo)constant_pool[classNameIndex-1]).name_index;
                if(constant_pool[nameIndex-1].tag == CPInfo.CONSTANT_Utf8){
                    return ((ConstantUtf8Info)constant_pool[nameIndex-1]).toString();
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    public String getMethodNameByMethodRef(int methodRefIndex){
        if(constant_pool[methodRefIndex-1].tag == CPInfo.CONSTANT_Methodref){
            int nameAndTypeIndex = ((ConstantMethodrefInfo)constant_pool[methodRefIndex-1]).name_and_type_index;
            if(constant_pool[nameAndTypeIndex-1].tag == CPInfo.CONSTANT_NameAndType){
                int methodNameIndex = ((ConstantNameAndTypeInfo)constant_pool[nameAndTypeIndex-1]).name_index;
                if(constant_pool[methodNameIndex-1].tag == CPInfo.CONSTANT_Utf8){
                    return ((ConstantUtf8Info)constant_pool[methodNameIndex-1]).toString();
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    public String getMethodParametersByMethodRef(int methodRefIndex){
        if(constant_pool[methodRefIndex-1].tag == CPInfo.CONSTANT_Methodref){
            int nameAndTypeIndex = ((ConstantMethodrefInfo)constant_pool[methodRefIndex-1]).name_and_type_index;
            if(constant_pool[nameAndTypeIndex-1].tag == CPInfo.CONSTANT_NameAndType){
                int descriptorIndex = ((ConstantNameAndTypeInfo)constant_pool[nameAndTypeIndex-1]).descriptor_index;
                if(constant_pool[descriptorIndex-1].tag == CPInfo.CONSTANT_Utf8){
                    return ((ConstantUtf8Info)constant_pool[descriptorIndex-1]).toString();
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public String getClassNameByClassInfoIndex(int classInfoIndex){
        if(constant_pool[classInfoIndex-1].tag == CPInfo.CONSTANT_Class){
            int name_index = ((ConstantClassInfo)constant_pool[classInfoIndex-1]).name_index;
            if(constant_pool[name_index-1].tag == CPInfo.CONSTANT_Utf8){
                return ((ConstantUtf8Info)constant_pool[name_index-1]).toString();
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    public String getTypeDescriptorByFieldrefIndex(int fieldRefIndex){
        if(constant_pool[fieldRefIndex-1].tag == CPInfo.CONSTANT_Fieldref){
            int nameAndTypeIndex = ((ConstantFieldrefInfo)constant_pool[fieldRefIndex-1]).name_and_type_index;
            if(constant_pool[nameAndTypeIndex-1].tag == CPInfo.CONSTANT_NameAndType){
                int descriptorIndex = ((ConstantNameAndTypeInfo)constant_pool[nameAndTypeIndex-1]).descriptor_index;
                if(constant_pool[descriptorIndex-1].tag == CPInfo.CONSTANT_Utf8){
                    return ((ConstantUtf8Info)constant_pool[descriptorIndex-1]).toString();
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    public int getFieldIndexByFieldrefIndex(int fieldRefIndex){
        int nameAndTypeIndex = ((ConstantFieldrefInfo)constant_pool[fieldRefIndex-1]).name_and_type_index;
        int nameIndex = ((ConstantNameAndTypeInfo)constant_pool[nameAndTypeIndex-1]).name_index;
        int i = 0;
        for(; i < fields_count; i++ ){
            if(fields[i].name_index == nameIndex)
                return i;
        }
        return -1;
    }
    
}
