/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vlload_n extends VInstruction{
    int n;
    
    public Vlload_n(int n){
        this.n = n;
    }

    @Override
    public void execute(VStackFrame frame) {
        long l = frame.loadLong(n);
        frame.pushOperandLong(l);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 1;
    }
}
