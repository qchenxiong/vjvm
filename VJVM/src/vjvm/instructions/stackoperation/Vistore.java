/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vistore extends VInstruction{
    byte vindex;

    public Vistore(byte byte1){
        this.vindex = byte1;
    }

    @Override
    public void execute(VStackFrame frame) {
        int i = frame.popOperandInt();
        frame.storeInt(vindex, i);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 2;
    }
}
