package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class ConstantNameAndTypeInfo extends CPInfo{

    short name_index;
    short descriptor_index;
    
    public ConstantNameAndTypeInfo(byte tag, DataInputStream stream) throws IOException{
        this.tag = tag;
        name_index = stream.readShort();
        descriptor_index = stream.readShort();
    }

    @Override
    public int getSize() {
        return 5;
    }
}
