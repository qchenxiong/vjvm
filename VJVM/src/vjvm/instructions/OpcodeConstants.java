/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions;

/**
 *
 * @author vin
 */
public class OpcodeConstants {
    //Stack and Local Variable Operations
    public final static int nop = 0;
    public final static int aconst_null = 1;
    public final static int iconst_m1 = 2;
    public final static int iconst_0 = 3;
    public final static int iconst_1 = 4;
    public final static int iconst_2 = 5;
    public final static int iconst_3 = 6;
    public final static int iconst_4 = 7;
    public final static int iconst_5 = 8;
    
    public final static int lconst_0 = 9;
    public final static int lconst_1 = 10;
    
    public final static int fconst_0 = 11;
    public final static int fconst_1 = 12;
    public final static int fconst_2 = 13;
    
    public final static int dconst_0 = 14;
    public final static int dconst_1 = 15;
    
    public final static int bipush = 16;
    public final static int sipush = 17;
    
    public final static int ldc = 18;
    public final static int ldc_w = 19;    
    public final static int ldc2_w = 20;
    
    public final static int pop = 87;
    public final static int pop2 = 88;
    public final static int dup = 89;
    public final static int dup_x1 = 90;
    public final static int dup_x2 = 91;
    public final static int dup2_x1 = 93;
    public final static int dup2_x2 = 94;
    public final static int swap = 95;
    
    public final static int iload = 21;
    public final static int iload_0 = 26;
    public final static int iload_1 = 27;
    public final static int iload_2 = 28;
    public final static int iload_3 = 29;
    
    public final static int fload = 23;
    public final static int fload_0 = 34;
    public final static int fload_1 = 35;
    public final static int fload_2 = 36;
    public final static int fload_3 = 37;
    
    public final static int lload = 22;
    public final static int lload_0 = 30;
    public final static int lload_1 = 31;
    public final static int lload_2 = 32;
    public final static int lload_3 = 33;
    
    public final static int dload = 24;
    public final static int dload_0 = 38;
    public final static int dload_1 = 39;
    public final static int dload_2 = 40;
    public final static int dload_3 = 41;
    
    public final static int aload = 25;
    public final static int aload_0 = 42;
    public final static int aload_1 = 43;
    public final static int aload_2 = 44;
    public final static int aload_3 = 45;
    
    public final static int istore = 54;
    public final static int istore_0 = 59;
    public final static int istore_1 = 60;
    public final static int istore_2 = 61;
    public final static int istore_3 = 62;
    
    public final static int fstore = 56;
    public final static int fstore_0 = 67;
    public final static int fstore_1 = 68;
    public final static int fstore_2 = 69;
    public final static int fstore_3 = 70;
    
    public final static int lstore = 55;
    public final static int lstore_0 = 63;
    public final static int lstore_1 = 64;
    public final static int lstore_2 = 65;
    public final static int lstore_3 = 66;
    
    public final static int dstore = 57;
    public final static int dstore_0 = 71;
    public final static int dstore_1 = 72;
    public final static int dstore_2 = 73;
    public final static int dstore_3 = 74;
    
    public final static int astore = 58;
    public final static int astore_0 = 75;
    public final static int astore_1 = 76;
    public final static int astore_2 = 77;
    public final static int astore_3 = 78;
    
    //Type Conversion
    public final static int i2l = 133;
    public final static int i2f = 134;
    public final static int i2d = 135;
    public final static int l2i = 136;
    public final static int l2f = 137;
    public final static int l2d = 138;
    public final static int f2i = 139;
    public final static int f2l = 140;
    public final static int f2d = 141;
    public final static int d2i = 142;
    public final static int d2l = 143;
    public final static int d2f = 144;
    public final static int i2b = 145;
    public final static int i2c = 146;
    public final static int i2s = 147;
    
    //integer Arithmetic
    public final static int iadd = 96;
    public final static int ladd = 97;
    public final static int iinc = 132;
    public final static int isub = 100;
    public final static int lsub = 101;
    public final static int imul = 104;
    public final static int lmul = 105;
    public final static int idiv = 108;
    public final static int ldiv = 109;
    public final static int irem = 112;
    public final static int lrem = 113;
    public final static int ineg = 116;
    public final static int lneg = 117;
    
    //float point arithmetic
    public final static int fadd = 98;
    public final static int dadd = 99;
    public final static int fsub = 102;
    public final static int dsub = 103;
    public final static int fmul = 106;
    public final static int dmul = 107;
    public final static int fdiv = 110;
    public final static int ddiv = 111;
    public final static int frem = 114;
    public final static int drem = 115;
    public final static int fneg = 118;
    public final static int dneg = 119;
    
    //logic operations
    public final static int ishl = 120;
    public final static int ishr = 122;
    public final static int iushr = 124;
    public final static int lshl = 121;
    public final static int lshr = 123;
    public final static int lushr = 125;
    public final static int iand = 126;
    public final static int ior = 128;
    public final static int ioxr = 130;
    public final static int land = 127;
    public final static int lor = 129;
    public final static int loxr = 131;
    
    //objects and arrays
    public final static int _new = 187;
    public final static int getfield = 180;
    public final static int putfield = 181;
    public final static int getstatic = 178;
    public final static int putstatic = 179;
    public final static int checkcast = 192;
    public final static int _instanceof = 193;
    public final static int newarray = 188;
    public final static int anewarray = 189;
    public final static int multianewarray = 197;
    public final static int arraylength = 190;
    public final static int baload = 51;
    public final static int caload = 52;
    public final static int saload = 53;
    public final static int iaload = 46;
    public final static int laload = 47;
    public final static int faload = 48;
    public final static int daload = 49;
    public final static int aaload = 50;
    public final static int bastore = 84;
    public final static int castore = 85;
    public final static int sastore = 86;
    public final static int iastore = 79;
    public final static int lastore = 80;
    public final static int fastore = 81;
    public final static int dastore = 82;
    public final static int aastore = 83;
    
    //constrol flow
    public final static int ifeq = 153;
    public final static int ifne = 154;
    public final static int iflt = 155;
    public final static int ifge = 156;
    public final static int ifgt = 157;
    public final static int ifle = 158;
    public final static int if_icmpeq = 159;
    public final static int if_icmpne = 160;
    public final static int if_icmplt = 161;
    public final static int if_icmpge = 162;
    public final static int if_icmpgt = 163;
    public final static int if_icmple = 164;
    public final static int lcmp = 148;
    public final static int fcmpg = 150;
    public final static int fcmpl = 149;
    public final static int dcmpg = 152;
    public final static int dcmpl = 151;
    public final static int ifnull = 198;
    public final static int ifnonnull = 199;
    public final static int if_acmpeq = 165;
    public final static int if_acmpne = 166;
    public final static int _goto = 167;
    public final static int goto_w = 200;
    public final static int lookupswitch = 171;
    public final static int tableswitch = 170;
    
    //exceptions
    public final static int athrow = 191;
    public final static int jsr = 168;
    public final static int jsr_w = 201;
    public final static int ret = 169;
    
    //methods
    public final static int invokevirtual = 182;
    public final static int invokespecial = 183;
    public final static int invokestatic = 184;
    public final static int invokeinterface = 185;
    public final static int ireturn = 172;
    public final static int lreturn = 173;
    public final static int freturn = 174;
    public final static int dreturn = 175;
    public final static int areturn = 176;
    public final static int _return = 177;
    
        
    //synchronization
    public final static int monitorenter = 194;
    public final static int monitorexit = 195;
    
    public final static int wide = 196;
}
