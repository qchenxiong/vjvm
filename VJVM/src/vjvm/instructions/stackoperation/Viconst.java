/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Viconst extends VInstruction{
    int n;
    
    public Viconst(int n){
        this.n = n;
    }

    @Override
    public void execute(VStackFrame frame) {
        frame.pushOperandInt(n);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 1;
    }
    
}
