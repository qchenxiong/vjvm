/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.runtime;

import java.util.HashMap;
import vjvm.classfile.JavaClass;

/**
 *
 * @author vin
 */
public class ClassHeap {
    HashMap<String, JavaClass> classMap;
    private static ClassHeap instance;
    
    private ClassHeap(){
        classMap = new HashMap<String, JavaClass>();
    }
    
    public static ClassHeap getInstance(){
        if(instance == null){
            instance = new ClassHeap();
        }
        return instance;
    }
    
    public void addClass(JavaClass jc){
        classMap.put(jc.getName(), jc);
    }
    
    public JavaClass getClass(String className){
        return classMap.get(className);
    }
}
