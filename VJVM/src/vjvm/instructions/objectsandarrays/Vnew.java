/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.objectsandarrays;

import vjvm.classfile.JavaClass;
import vjvm.instructions.VInstruction;
import vjvm.runtime.ClassHeap;
import vjvm.runtime.VExecutor;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vnew extends VInstruction{
    byte indexbyte1, indexbyte2;
    
    public Vnew(byte byte1, byte byte2){
        this.indexbyte1 = byte1;
        this.indexbyte2 = byte2;
    }

    @Override
    public void execute(VStackFrame frame) {
        int classInfoIndex = ((int)indexbyte1 << 8) + ((int)indexbyte2 & 0xff);
        String className = VExecutor.currentJC.getClassNameByClassInfoIndex(classInfoIndex);
        JavaClass jc = ClassHeap.getInstance().getClass(className);
        if(jc == null){
            System.err.println("class "+className+" not loaded yet");
            System.exit(0);
        }
        int objRef = jc.createInstance();
        frame.pushOperandReference(objRef);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 3;
    }
  
    
}
