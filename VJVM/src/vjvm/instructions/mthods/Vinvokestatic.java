/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.mthods;

import java.util.ArrayList;
import vjvm.classfile.JavaClass;
import vjvm.instructions.VInstruction;
import vjvm.runtime.ClassHeap;
import vjvm.runtime.VExecutor;
import vjvm.runtime.VStackFrame;
import vjvm.type.*;

/**
 *
 * @author vin
 */
public class Vinvokestatic extends VInstruction{
    byte indexbyte1, indexbyte2;
    
    public Vinvokestatic(byte byte1, byte byte2){
        this.indexbyte1 = byte1;
        this.indexbyte2 = byte2;
    }

    @Override
    public void execute(VStackFrame frame) {
        int methodRefIndex = (indexbyte1 << 8) + (((int)indexbyte2) & 0xff);
        
        //get the Class the method belongs to
        String className = VExecutor.currentJC.getClassNameByMethodRef(methodRefIndex);
        JavaClass jc = ClassHeap.getInstance().getClass(className);
        
        //get the methodIndex
        String methodName = VExecutor.currentJC.getMethodNameByMethodRef(methodRefIndex);
        int methodIndex = jc.getMethodIndex(methodName);
        
        //get the method parameters
        String parametersString = VExecutor.currentJC.getMethodParametersByMethodRef(methodRefIndex);
        ArrayList<VType> parameters = VType.getTypeList(parametersString);
        for(int i = parameters.size() - 1; i >= 0; i--){
            switch(parameters.get(i).descriptor.charAt(0)){
                case 'B':
                    ByteType bt = (ByteType) parameters.get(i);
                    bt.setValue(frame.popOperandByte());
                    break;
                case 'C':
                    CharType ct = (CharType) parameters.get(i);
                    ct.setValue(frame.popOperandChar());
                    break;
                case 'D':
                    DoubleType dt = (DoubleType) parameters.get(i);
                    dt.setValue(frame.popOperandDouble());
                    break;
                case 'F':
                    FloatType ft = (FloatType) parameters.get(i);
                    ft.setValue(frame.popOperandFloat());
                    break;
                case 'I':
                    IntType it = (IntType) parameters.get(i);
                    it.setValue(frame.popOperandInt());
                    break;
                case 'J':
                    LongType lt = (LongType) parameters.get(i);
                    lt.setValue(frame.popOperandLong());
                    break;
                case 'L':
                    ReferenceType rt = (ReferenceType) parameters.get(i);
                    rt.setValue(frame.popOperandReference());
                    break;
                case 'S':
                    ShortType st = (ShortType) parameters.get(i);
                    st.setValue(frame.popOperandShort());
                    break;
                case 'Z':
                    BooleanType booleanType= (BooleanType) parameters.get(i);
                    booleanType.setValue(frame.popOperandBoolean());
                    break;
            }
        }
        
        VExecutor.getInstance().invokeStatic(jc, methodIndex, parameters);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 3;
    }
    
}
