/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.runtime;

import vjvm.classfile.JavaClass;
import vjvm.type.VType;

/**
 *
 * @author vin
 */
public class VObject {
    JavaClass jc;
    VType fields[];
    
    public VObject(JavaClass jc, VType fields[]){
        this.jc = jc;
        this.fields = fields;
    }
    
    public JavaClass getJavaClass(){
        return jc;
    }
    
    public VType getField(int index){
        return fields[index];
    }
}
