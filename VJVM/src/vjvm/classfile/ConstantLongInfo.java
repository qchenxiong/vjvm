package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-18
 * @author vincent
 */
public class ConstantLongInfo extends CPInfo{

    long value;
    
    public ConstantLongInfo(byte tag, DataInputStream stream) throws IOException{
        this.tag = tag;
        value = stream.readLong();
    }

    @Override
    public int getSize() {
        return 9;
    }
}
