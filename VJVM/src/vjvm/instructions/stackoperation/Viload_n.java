/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Viload_n extends VInstruction{
    int n;
    
    public Viload_n(int n){
        this.n = n;
    }

    @Override
    public void execute(VStackFrame frame) {
        int i = frame.loadInt(n);
        frame.pushOperandInt(i);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 1;
    }
}
