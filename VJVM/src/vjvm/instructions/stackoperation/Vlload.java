/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vlload extends VInstruction{
    byte vindex;

    public Vlload(byte byte1){
        this.vindex = byte1;
    }

    @Override
    public void execute(VStackFrame frame) {
        long l = frame.loadLong(vindex);
        frame.pushOperandLong(l);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 2;
    }
}
