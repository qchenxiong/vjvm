package vjvm.classfile;

/**
 * @date 2012-10-18
 * @author vincent
 */
public abstract class CPInfo {
    public final static int CONSTANT_Class = 7;
    public final static int CONSTANT_Fieldref = 9;
    public final static int CONSTANT_Methodref = 10;
    public final static int CONSTANT_InterfaceMethodref = 11;
    public final static int CONSTANT_String = 8;
    public final static int CONSTANT_Integer = 3;
    public final static int CONSTANT_Float = 4;
    public final static int CONSTANT_Long = 5;
    public final static int CONSTANT_Double = 6;
    public final static int CONSTANT_NameAndType = 12;
    public final static int CONSTANT_Utf8 = 1;
    
    byte tag;
    
    public abstract int getSize();
}
