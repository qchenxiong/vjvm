/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vsipush extends VInstruction{
    byte byte1, byte2;

    public Vsipush(byte byte1, byte byte2){
        this.byte1 = byte1;
        this.byte2 = byte2;
    }

    @Override
    public void execute(VStackFrame frame) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getByteSize() {
        return 3;
    }
}
