/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.controlflow;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vif_icmpge extends VInstruction{
    byte branchbyte1, branchbyte2;
    int offset;
    
    public Vif_icmpge(byte byte1, byte byte2){
        this.branchbyte1 = byte1;
        this.branchbyte2 = byte2;
        offset = ((int)byte1 << 8) + (((int)byte2) & 0xff);
    }

    @Override
    public void execute(VStackFrame frame) {
        int i1 = frame.popOperandInt();
        int i2 = frame.popOperandInt();
        if(i1 == i2){
            frame.jump(offset);
        }else{
            frame.forward();
        }
    }

    @Override
    public int getByteSize() {
        return 3;
    }
    
    
}
