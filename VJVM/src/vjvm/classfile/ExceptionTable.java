package vjvm.classfile;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @date 2012-10-19
 * @author vincent
 */
public class ExceptionTable {
    
    short start_pc;
    short end_pc;
    short handler_pc;
    short catch_type;
    
    public ExceptionTable(DataInputStream stream) throws IOException{
        start_pc = stream.readShort();
        end_pc = stream.readShort();
        handler_pc = stream.readShort();
        catch_type = stream.readShort();
    }
    
    public int getSize(){
        return 8;
    }
}
