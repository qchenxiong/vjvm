<TeXmacs|1.0.7>

<style|article>

<\body>
  <doc-data|<doc-title|The VJVM Specification>>

  <section|Introduction>

  \;

  This is a simple implement of the JVM with java, I plan to implement it
  follow the tutorial of ``KJVM'' which is a C<math|\<upl\>\<upl\>>
  implementation, you can find it at ``Code Project''.

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|language|british>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
  </collection>
</references>