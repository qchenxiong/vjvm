/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.mthods;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStack;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vlreturn extends VInstruction{

    @Override
    public void execute(VStackFrame frame) {
        long l = frame.popOperandLong();
        int i = VStack.getInstance().getFrameIndex(frame);
        VStack.getInstance().getFrame(i-1).pushOperandLong(l);
        frame.stop();
        VStack.getInstance().popFrame(frame);
    }

    @Override
    public int getByteSize() {
        return 1;
    }
    
}
