/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vjvm.instructions.stackoperation;

import vjvm.instructions.VInstruction;
import vjvm.runtime.VStackFrame;

/**
 *
 * @author vin
 */
public class Vaload_n extends VInstruction{
    int n;
    
    public Vaload_n(int n){
        this.n = n;
    }

    @Override
    public void execute(VStackFrame frame) {
        int objRef = frame.loadReference(n);
        frame.pushOperandReference(objRef);
        frame.forward();
    }

    @Override
    public int getByteSize() {
        return 1;
    }
}
